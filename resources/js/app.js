require('./bootstrap');

window.Vue = require('vue');

// Vue.component('slider', require('./components/Slider.vue').default);

// const Slider = require('./components/Slider.vue').default;

import Slider from './components/Slider.vue';

new Vue({
    el: "#app",
    components: {
        'slider': Slider,
    }
});
